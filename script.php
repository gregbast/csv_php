<?php

$servername = "localhost";
$username = "";
$password = "";
$database = "simplon_test";
try {
    $conn = new PDO("mysql:host$servername;dbname=$database", $username, $password);
} catch (PDOException $e) {
    echo 'error' . $e->getMessage();
    die();
}
?>








<?php
$csv = [];
array_push($csv, 'id', 'firstname', 'lastname', 'email', 'profession', 'birthdate', 'country', 'phone');
$update = fopen("people-clean.csv", "wr");
// foreach ($csv as $resdata) {
//     $res = explode(",", $resdata);
//     fputcsv($update, $res);
// }
fputcsv(
    $update,
    $csv,
    $separator = ",",
    $enclosure = '"',
    $escape = "\\",

);



$handle = fopen($_POST["get_file"], "r");
while (($data = fgetcsv($handle)) !== FALSE) {
    if ($data[0] == "id") {
        continue;
    }

    $id = $data[0];
    $first_name = $data[1];
    $last_name = $data[2];
    $mail = filter_var($data[3], FILTER_VALIDATE_EMAIL) ? strtolower($data[3]) : NULL;

    $metier = $data[4];
    if ($metier == "politicien")
        $metier = "menteur";

    $bd = DATE('Y-m-d', strtotime($data[5]));

    $tel = "0" . substr($data[7], -9);

    $country = ($data[6] == 'CS' ? "tchequie" : ($data[6] == 'AN' ? "anti neerlandaise" : NULL));

    if ($country == NULL) {

        $info_api = file_get_contents('https://restcountries.com/v3.1/alpha/' . $data[6] . '/?fields=name');
        $info_api = json_decode($info_api);
        $country = $info_api->name->common;
    }
    try {
        $sql = $conn->prepare("INSERT INTO simplon_test.people (id, firstname, lastname,email, profession, birthdate,country,phone) VALUES (:insert_id,:insert_fn,:insert_ln,:insert_mail,:insert_pro,:insert_bd,:insert_country,:insert_tel)");
        $sql->bindparam('insert_id', $id, PDO::PARAM_INT);
        $sql->bindparam('insert_fn', $first_name, PDO::PARAM_STR);
        $sql->bindparam('insert_ln', $last_name, PDO::PARAM_STR);
        $sql->bindparam('insert_mail', $mail, PDO::PARAM_STR);
        $sql->bindparam('insert_pro', $metier, PDO::PARAM_STR);
        $sql->bindparam('insert_bd', $bd, PDO::PARAM_STR);
        $sql->bindparam('insert_country', $country, PDO::PARAM_STR);
        $sql->bindparam('insert_tel', $tel, PDO::PARAM_STR);
        $sql->execute();
    } catch (PDOException $e) {
        echo 'error' . $e->getMessage();
        die();
    }

    $csv = [];
    array_push($csv, $id, $first_name, $last_name, $mail, $metier, $bd, $country, $tel);


    fputcsv(
        $update,
        $csv,
        $separator = ",",
        $enclosure = '"',
        $escape = "\\",

    );
}
fclose($update);
echo "finish";
?>